Yii2 Sortable GridView
======================
Yii2 Sortable GridView

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist kfit/yii2-sortable-gridview "*"
```

or add

```
"kfit/yii2-sortable-gridview": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \kfit\sortablegridview\SortableGridView::widget(); ?>```